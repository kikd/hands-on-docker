import * as Express from 'express'

import getMessages from './getMessages'
import postMessage from './postMessage'

const API_VERSION = '/v1'

const router = Express.Router({ mergeParams: true })

router.get(API_VERSION + '/messages', getMessages)
router.post(API_VERSION + '/messages', postMessage)

export default router
