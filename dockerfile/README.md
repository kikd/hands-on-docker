# Dockerfileを書いてみる

## Docker imageをbuildしてみる

```
$ docker build -t vuetify-material-dashboard .
$ docker run -d --rm --name vuetify-material-dashboard -p 80:8080 vuetify-material-dashboard
$ docker logs -f vuetify-material-dashboard
```

- `http://localhost` に アクセスして確認
- コンテナを停止

```
$ docker stop vuetify-material-dashboard
```

------

- `dockerfile` の中の git clone のURLを変更する
  - `https://github.com/vuetifyjs/theme-freelancer.git`
  - それに合わせて、`WORKDIR` や `VOLUME` も修正する
- 再度ビルド

```
$ docker build -t theme-freelancer .
$ docker run -d --rm --name theme-freelancer -p 80:8080 theme-freelancer
$ docker logs -f theme-freelancer
```

- `http://localhost` に アクセスして確認
- コンテナを停止

```
$ docker stop theme-freelancer
```
